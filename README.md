# NKTool

[![CI Status](https://img.shields.io/travis/shangzhenxing/NKTool.svg?style=flat)](https://travis-ci.org/shangzhenxing/NKTool)
[![Version](https://img.shields.io/cocoapods/v/NKTool.svg?style=flat)](https://cocoapods.org/pods/NKTool)
[![License](https://img.shields.io/cocoapods/l/NKTool.svg?style=flat)](https://cocoapods.org/pods/NKTool)
[![Platform](https://img.shields.io/cocoapods/p/NKTool.svg?style=flat)](https://cocoapods.org/pods/NKTool)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NKTool is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NKTool'
```

## Author

shangzhenxing, zhenxing.developer@outlook.com

## License

NKTool is available under the MIT license. See the LICENSE file for more info.
