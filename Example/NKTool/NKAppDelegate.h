//
//  NKAppDelegate.h
//  NKTool
//
//  Created by shangzhenxing on 04/08/2022.
//  Copyright (c) 2022 shangzhenxing. All rights reserved.
//

@import UIKit;

@interface NKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
