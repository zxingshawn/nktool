//
//  main.m
//  NKTool
//
//  Created by shangzhenxing on 04/08/2022.
//  Copyright (c) 2022 shangzhenxing. All rights reserved.
//

@import UIKit;
#import "NKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NKAppDelegate class]));
    }
}
